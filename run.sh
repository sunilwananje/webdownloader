#!/bin/bash 
php artisan cache:clear
php artisan config:clear
php artisan clear-compiled
php artisan config:cache
php artisan queue:restart
chmod -R 777 .
