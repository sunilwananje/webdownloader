<script src="{{ asset('assets/admin/js/plugins/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/plugins/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/plugins/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/scripts/script.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/scripts/sidebar.large.script.min.js') }}"></script>
<script type="text/javascript">
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
