<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersDownload extends Model
{
    protected $fillable = ['uuid', 'url', 'pages', 'theme_path', 'user_id', 'status'];
}
