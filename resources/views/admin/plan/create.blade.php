@extends('admin.layouts.default')
@section('title') {{ __('Users') }} @endsection
@section('custom-css')
<link href="{{ asset('assets/admin/css/plugins/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@if ($plan)
<?php $title = 'Edit Plan';?>
<?php $route = route('admin.users.update', $plan->id);?>
@else
<?php $title = 'Add Plan';?>
<?php $route = route('admin.users.store');?>
@endif
    <div class="breadcrumb">
        <h1 class="mr-2">Users</h1>
        <ul>
            <li><a href="{{ route('admin.dashboard')}}">Dashboard</a></li>
            <li><a href="{{ route('admin.plan.index')}}">Plan</a></li>
            <li>{{ $title }}</li>
        </ul>
        <span class="flex-grow-1"></span>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row">
        <div class="col-md-4">
            <h4>{{ $title }}</h4>
            <p>A form control layout using row with left label alignment.</p>
            <div class="card mb-3">
                <div class="card-body">
                    <form action="{{ route('admin.plan.store')}}" class="needs-validation" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="name">Name</label>
                            <input class="form-control {{ ($errors->has('name') ?  'is-invalid' : '') }}" id="name" type="text" name="name" placeholder="Name" value="{{ old('name')}}" required="required">
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3">Description</label>
                            <textarea class="form-control {{ ($errors->has('email')) ?  'is-invalid' : '' }}" name="description" id="description" placeholder="Description" required>{{ old('description') }}</textarea>
                            @error('description')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3">Price</label>
                            <input class="form-control {{ ($errors->has('price')) ?  'is-invalid' : '' }}" name="price" id="price" type="text" placeholder="Price" required>
                            @error('price')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="interval_unit">Interval</label>
                            <select class="form-control {{ ($errors->has('interval_unit')) ?  'is-invalid' : '' }}" name="interval_unit" id="interval_unit">
                                <option value="">Select</option> 
                                <option value="month">Month</option> 
                                <option value="year">Year</option> 
                            </select>
                            @error('interval_unit')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="sort_order">Sort Order</label>
                            <input class="form-control {{ ($errors->has('sort_order')) ?  'is-invalid' : '' }}" id="sort_order" type="number" name="sort_order" placeholder="Sort Order" value="{{ old('sort_order') }}" required="required">
                            @error('sort_order')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h4>Add Feature</h4>
            <p>A form control layout using row with left label alignment.</p>
            <div class="card mb-3">
                <div class="card-body">
                    <form action="{{ route('admin.feature.store')}}" class="needs-validation" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="name">Name</label>
                            <input class="form-control {{ ($errors->has('name')) ?  'is-invalid' : '' }}" id="name" type="text" name="name" placeholder="Name" value="{{ old('name') }}" required="required">
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3">Description</label>
                            <textarea class="form-control {{ ($errors->has('email')) ?  'is-invalid' : '' }}" name="description" id="description" placeholder="Description" required>{{ old('description', $plan->description) }}</textarea>
                            @error('description')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="interval_unit">Interval</label>
                            <select class="form-control {{ ($errors->has('interval_unit')) ?  'is-invalid' : '' }}" name="interval_unit" id="interval_unit">
                                <option value="">Select</option> 
                                <option value="day">Day</option> 
                                <option value="week">Week</option> 
                                <option value="month">Month</option> 
                                <option value="year">Year</option> 
                            </select>
                            @error('interval_unit')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="sort_order">Sort Order</label>
                            <input class="form-control {{ ($errors->has('sort_order')) ?  'is-invalid' : '' }}" id="sort_order" type="number" name="sort_order" placeholder="Sort Order" value="{{ old('sort_order') }}" required="required">
                            @error('sort_order')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h4>Add Features Value</h4>
            <p>A form control layout using row with left label alignment.</p>
            <div class="card mb-3">
                <div class="card-body">
                    <form action="{{ route('admin.featureValue.store')}}" class="needs-validation" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="inputEmail3">Plan</label>
                            <select class="form-control {{ ($errors->has('plan_id')) ?  'is-invalid' : '' }}" name="plan_id" id="plan_id">
                            <option value="">Select</option> 
                            @foreach($plans as $plan)
                                <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                            @endforeach
                            </select>
                            @error('plan_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3">Feature</label>
                            <select class="form-control {{ ($errors->has('feature_id')) ?  'is-invalid' : '' }}" name="feature_id" id="feature_id">
                                <option value="">Select</option> 
                                @foreach($features as $feature)
                                    <option value="{{ $feature->id }}">{{ $feature->name }}</option>
                                @endforeach
                            </select>
                            @error('feature_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3">Value</label>
                            <input class="form-control {{ ($errors->has('value')) ?  'is-invalid' : '' }}" name="value" id="value" type="text" placeholder="Value" required>
                            @error('value')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3">Note</label>
                            <textarea class="form-control {{ ($errors->has('note')) ?  'is-invalid' : '' }}" name="note" id="note" placeholder="Note" required>{{ old('note') }}</textarea>
                            @error('note')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main-content -->
@endsection

@section('custom-script')
<script src="{{ asset('assets/admin/js/plugins/sweetalert2.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
  $(document).on('click', '#addRow', function () {
      $("#firstRow").clone().appendTo("featureForm");
  });
});
</script>
@endsection