@extends('admin.layouts.default')
@section('title') {{ __('Users') }} @endsection
@section('custom-css')
<link href="{{ asset('assets/admin/css/plugins/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@if ($user)
<?php $title = 'Edit User';?>
<?php $route = route('admin.users.update', $user->id);?>
@else
<?php $title = 'Add User';?>
<?php $route = route('admin.users.store');?>
@endif
    <div class="breadcrumb">
        <h1 class="mr-2">Users</h1>
        <ul>
            <li><a href="{{ route('admin.dashboard')}}">Dashboard</a></li>
            <li><a href="{{ route('admin.users.index')}}">Users</a></li>
            <li>{{ $title }}</li>
        </ul>
        <span class="flex-grow-1"></span>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row">
        <div class="col-md-8">
            <h4>{{ $title }}</h4>
            <p>A form control layout using row with left label alignment.</p>
            <div class="card mb-5">
                <div class="card-body">
                    <form action="{{ $route }}" class="needs-validation" method="POST">
                        @csrf
                        @if ($user)
                            @method('PATCH')
                        @endif
                        <!-- class="col-sm-2 col-form-label" -->
                        <div class="form-group row">
                            <label for="name">Name</label>
                            <input class="form-control {{ ($errors->has('name')) ?  'is-invalid' : '' }}" id="name" type="text" name="name" placeholder="Name" value="{{ old('name', $user->name) }}" required="required">
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3">Email</label>
                            <input class="form-control {{ ($errors->has('email')) ?  'is-invalid' : '' }}" name="email" id="email" type="email" placeholder="Email" value="{{ old('email', $user->email) }}" required="required">
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3">Password</label>
                            <input class="form-control {{ ($errors->has('password')) ?  'is-invalid' : '' }}" name="password" id="password" type="password" placeholder="Password" required="required">
                            @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="password_confirmation">Confirm Password</label>
                            <input class="form-control {{ ($errors->has('password')) ?  'is-invalid' : '' }}" name="password_confirmation" id="password_confirmation" type="password" placeholder="Confirm Password" required="required">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main-content -->
@endsection

@section('custom-script')
<script src="{{ asset('assets/admin/js/plugins/sweetalert2.min.js') }}"></script>
<!-- <script type="text/javascript">
$(document).ready(function () {
  'use strict'; // Fetch all the forms we want to apply custom Bootstrap validation styles to

  var forms = document.getElementsByClassName('needs-validation'); // Loop over them and prevent submission

  var validation = Array.prototype.filter.call(forms, function (form) {
    form.addEventListener('submit', function (event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }

      form.classList.add('was-validated');
    }, false);
  });
});
</script> -->
@endsection