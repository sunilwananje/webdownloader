<?php

namespace App\Repositories;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Models\UsersDownload;
use App\Repositories\Interfaces\UsersDownloadRepositoryInterface;
use File,Storage;

class UsersDownloadRepository implements UsersDownloadRepositoryInterface
{
	private $host;
	private $scheme;
	private $domain;
	private $fullDomain;
	private $rootDir;
	private $fileName;
	private $firstPageUrl;

	// Get all instances of model
	public function all($take = null)
    {
    	$records = UsersDownload::all();
    	if($take) {
    		$records->take($take);
    	}
        return $records;
    }

    public function update($id, array $attributes)
	{
	  return UsersDownload::find($id)->update($attributes);
	}

    // Get by user
    public function getByUser(User $user, $take = null)
    {
    	$records = UsersDownload::where('user_id', $user->id)->orderBy('id', 'desc');
    	if($take) {
    		$records->take($take);
    	}
        return $records->get();
    }

    // remove record from the database
    public function delete($id)
    {
        //return UsersDownload::destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        //return UsersDownload::findOrFail($id);
    }

    // create a new record in the database
    public function download(UsersDownload $usersDownload)
    {
    	try {
    		$extractedLinks = $this->extractData($usersDownload->url, $usersDownload);
    		$links = $extractedLinks['href'];
			if($usersDownload->pages == 1) {
				$downloadedLinks = []; 
				foreach ($links as $key => $link) {
					if(!in_array($link, $downloadedLinks)) {
						$result = $this->extractData($link, $usersDownload, 'yes');
						$downloadedLinks[] = $link;
					}
				}
			}
			return $this->firstPageUrl;
    	} catch(\Exception $e) {
    		Log::error('FileError:' . $e->getMessage());
    		//return null;
    	}
    }

    private function getProxies()
	{
		$proxies = []; // Declaring an array to store the proxy list
 
		// Adding list of proxies to the $proxies array
		$proxies[] = '45.72.30.159';  // Some proxies require user, password, IP and port number
		$proxies[] = '209.127.191.180';
		$proxies[] = '45.130.255.198';
		$proxies[] = '193.8.127.117';  // Some proxies only require IP
		$proxies[] = '193.8.215.243';
		$proxies[] = '45.130.125.157';
		$proxies[] = '45.130.255.243';
		$proxies[] = '193.8.215.243';
		$proxies[] = '185.164.56.246';
		$proxies[] = '45.136.231.226';
		return $proxies;
	}

	// get content of pages from url
	private function getContent($url)
	{
		//The username for authenticating with the proxy.
		$proxyUsername = 'icecadwm-dest';
		 
		//The password for authenticating with the proxy.
		$proxyPassword = 'rfxrgo5m9oa2';
		$agents = array(
			'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
			'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.9) Gecko/20100508 SeaMonkey/2.0.4',
			'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
			'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1',
			'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
		);
		
		$proxies = $this->getProxies();
		// if (!empty($proxies)) {  // If the $proxies array contains items, then
		//     $proxy = $proxies[array_rand($proxies)];    // Select a random proxy from the array and assign to $proxy variable
		//     echo '$proxy='.$proxy;
		//     $port = '80';//array_search($proxy, $proxies);
		//     //die($port.'---'.$proxy);
		// }
	
		// Set any other cURL options that are required
		$options[CURLOPT_URL] = $url;
		$options[CURLOPT_HEADER] = FALSE;
		$options[CURLOPT_USERAGENT] = $agents[array_rand($agents)];
		$options[CURLOPT_SSL_VERIFYPEER] = FALSE;
		$options[CURLOPT_COOKIESESSION] = TRUE;
		$options[CURLOPT_FOLLOWLOCATION] = TRUE;
		$options[CURLOPT_RETURNTRANSFER] = TRUE;

		//Specify the username and password.
		//$options[CURLOPT_PROXYUSERPWD] = "$proxyUsername:$proxyPassword";

		//Setting proxy option for cURL
		if (isset($proxy)) {
			$options[CURLOPT_PROXY] = $proxy;// Set CURLOPT_PROXY with proxy in $proxy variable
			$options[CURLOPT_PROXYPORT] = $port;//Set the port.
		}
		$ch = curl_init(); $ch = curl_init();  // Initialise a cURL handle
		curl_setopt_array($ch, $options);
		 
		$html = curl_exec($ch);  // Execute a cURL request
		$curlInfo = curl_getinfo($ch);

		if (curl_errno($ch)) {
		    $error_msg = curl_error($ch);
		}

		curl_close($ch);

		if (isset($error_msg)) {
		    throw new \ErrorException($error_msg);
		}

	    return $html;
	}

	//filter url
	private function getDomain($url)
	{
		$this->fileName = 'index.html';
		$tempUrl = $url;
		if(substr($url, -1) === '/') {
			$tempUrl = $url . 'index.html';
		}
		$parse = $this->getParseUrl($tempUrl);

		$this->scheme = $parse['scheme'];
		$this->host = $parse['host'];
		$this->domain = $parse['host'];
		if(isset($parse['path'])) {
			$mainUrlInfo = pathinfo($parse['path']);
			if(isset($mainUrlInfo['filename']) && !empty($mainUrlInfo['filename'])){
				$this->fileName = $mainUrlInfo['filename'] .'.html';
			}
			if(isset($mainUrlInfo['dirname']) && !empty($mainUrlInfo['dirname']) && $mainUrlInfo['dirname']!='.'){
				$this->domain = $parse['host'] .'/'.$mainUrlInfo['dirname'];
			}
		}

		$this->fullDomain = $this->scheme.'://'.$this->domain;

		return $this->domain;
	}

	//separate url parts
	private function getParseUrl($url, $domain = null)
	{
		$parseUrl = parse_url($url);
		if(isset($parseUrl['path'])) {
			if($parseUrl['path'][0] === '/') {
				$parseUrl['path'] = ltrim($parseUrl['path'], '/');
			}
		}

		$parseUrl['scheme'] = (array_key_exists('scheme', $parseUrl) ? $parseUrl['scheme'] : 'http');
		$parseUrl['host'] = (array_key_exists('host', $parseUrl) ? $parseUrl['host'] : $domain);

		return $parseUrl;
	}

	//get absolute path
	private function getAbsolutePath($path)
	{
	    $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
	    $absolutes = array();
	    foreach ($parts as $part) {
	        if ('.' == $part) continue;
	        if ('..' == $part) {
	            array_pop($absolutes);
	        } else {
	            $absolutes[] = $part;
	        }
	    }
	    return implode(DIRECTORY_SEPARATOR, $absolutes);
	}

	//get images, css, javascript, pages links
	private function extractData($url, $downloadObj, $isAll = null)
	{
		//$url = $downloadObj->url;
		$this->domain = $this->getDomain($url);//file_get_contents($url);

    	$this->rootDir = 'uid_'.$downloadObj->user_id.'/'.$downloadObj->uuid.'/'.$this->domain;
    	//Storage::copy('old/file.jpg', 'new/file.jpg');
		$html = $this->getContent($url);//file_get_contents($url);
	
		//Instantiate the DOMDocument class.
		$htmlDom = new \DOMDocument;
		 
		//Parse the HTML of the page using DOMDocument::loadHTML
		@$htmlDom->loadHTML($html);
		 
		//Array that will contain our extracted links.
		$extractedLinks = array();
		 
		//extract img source link
		$imgs = $htmlDom->getElementsByTagName('img');
		foreach($imgs as $link){
		    $linkHref = $link->getAttribute('src');
		    if (strlen(trim($linkHref)) == 0) {
		        continue;
		    }
		    
			try {
			    $filePath = $this->storeFiles($this->domain, $linkHref);
				$link->setAttribute('src', $filePath);
			} catch(\Exception $e) {
	    		Log::error('Image file Error of ' . $linkHref . ':' . $e->getMessage());
	    		continue;
	    	}

		    $extractedLinks['img_src'][] = $linkHref;
		   // $extractedLinks['filePath'][] = $filePath;
		}
		Log::info("img_src:" . print_r($extractedLinks['img_src'], true));

		//extract css source link
		$links = $htmlDom->getElementsByTagName('link');
		foreach($links as $link) {
		    $linkHref = $link->getAttribute('href');
		    if (strlen(trim($linkHref)) == 0) {
		        continue;
		    }
		    $cssParse = parse_url($linkHref);
		    if(isset($cssParse['host']) && $cssParse['host'] != $this->host) {
			    continue;
			}
			try {
			    $filePath = $this->storeFiles($this->domain, $linkHref, 'css');
				$link->setAttribute('href', $filePath);
			} catch(\Exception $e) {
	    		Log::error('CSS file Error of ' . $linkHref . ':' . $e->getMessage());
	    		continue;
	    	}
    		//return null;
		    $extractedLinks['css'][] = $linkHref;
		}
		Log::info("css:" . print_r($extractedLinks['css'], true));

		//extract js source link
		$scripts = $htmlDom->getElementsByTagName('script');
		foreach($scripts as $k => $link){
		    $linkHref = $link->getAttribute('src');
		    if (strlen(trim($linkHref)) == 0) {
		        continue;
		    }
		    try {
			    $filePath = $this->storeFiles($this->domain, $linkHref);
				$link->setAttribute('src', $filePath);
			} catch(\Exception $e) {
	    		Log::error('JS file Error of ' . $linkHref . ':' . $e->getMessage());
	    		continue;
	    	}
		    
		    $extractedLinks['js'][] = $linkHref;	 
		}
		Log::info("js:" . print_r($extractedLinks['js'], true));

		//extract inline css images
		$pattern = '/url\(\s*[\'"]?(\S*\.(?:jpe?g|gif|png))[\'"]?\s*\)[^;}]*?/i';
		preg_match_all($pattern, $html, $match);
		foreach($match[1] as $imgUrl) {
			try {
			    $this->storeFiles($this->domain, $imgUrl);
			} catch(\Exception $e) {
	    		Log::error('Inline css image file Error of ' . $imgUrl . ':' . $e->getMessage());
	    		continue;
	    	}
	    }

		//Extract the links from the HTML.
		$links = $htmlDom->getElementsByTagName('a');
		//extract anchor tag link
		foreach($links as $link){
		    //Get the link text.
		    $linkText = $link->nodeValue;
		    //Get the link in the href attribute.
		    $linkHref = $link->getAttribute('href');
		    //If the link is empty, skip it and don't
		    //add it to our $extractedLinks array
		    if(strlen(trim($linkHref)) == 0){
		        continue;
		    }
		 
		    //Skip if it is a hashtag / anchor link.
		    if($linkHref[0] == '#' ||  strpos($linkHref, 'javascript:') !== false || strpos($linkHref, 'tel:') !== false || strpos($linkHref, 'mailto:') !== false) {
		    	$extractedLinks['skiped_links'][] = $linkHref;
		        continue;
		    }
		    $anchorParse = parse_url($linkHref);
		    // /print_r($anchorParse);
		    if(isset($anchorParse['host'])) {
		    	if($anchorParse['host'] != $this->host) {
				    $extractedLinks['external_link'][] = $linkHref;
				    continue;
				} else if($anchorParse['host'] == $this->host) {
					$linkHref = str_replace($this->fullDomain.'/', '', $linkHref);
				}
		    }
		    
		    $domainUrl = '';
		    $path = pathinfo($linkHref);
		    $tmpfileName = $path['filename'].'.html';

		    $newHref = str_replace($path['basename'], $tmpfileName, $linkHref);

		    $link->setAttribute('href', $newHref);
		    
			if (! filter_var($linkHref, FILTER_VALIDATE_URL)) {
			    $linkHref =  $this->scheme . '://' . $this->domain . '/' . $linkHref;
			}
			
			//$extractedLinks['internal_link'][] = $linkHref;
		    $extractedLinks['href'][] = $linkHref;
		}
		
		$html = $htmlDom->saveHTML();
		$stored = Storage::disk('preview')->put($this->rootDir.'/'.$this->fileName, $html, 'public');
		if(!$isAll && $stored) {
			$this->firstPageUrl = Storage::disk('preview')->url($this->rootDir.'/'.$this->fileName);
		}
		return $extractedLinks;
	}

	// store css, images, javascript files
	private function storeFiles($domain, $file, $type = null)
	{
		$parseUrl = $this->getParseUrl($file, $domain);
		if (isset($parseUrl['path'])) {
			$filePath = 'preview/' . $this->rootDir . '/' . $parseUrl['path'];
			$storagePath = storage_path($filePath);
			if (!File::exists($storagePath)) {
				
				$pathInfo = pathinfo($parseUrl['path']);
				$directory = storage_path('preview/'.$this->rootDir . '/' . $pathInfo['dirname']);
		    	$fileUrl = $parseUrl['scheme'] . '://' . $parseUrl['host'] . '/' . $parseUrl['path'];
		    	
	    		File::makeDirectory($directory, 0775, true, true);
		    	
				if (!copy($fileUrl, $storagePath)) {
				    Log::info("failed to copy=> source:$fileUrl, destination: $storagePath");
				}
				
				//Storage::disk('public')->copy($fileUrl, $filePath);
				if ($type == 'css') {
					//$cssFile = file_get_contents($fileUrl);
					$cssFile = $this->getContent($fileUrl);
					$pattern = '/url\(([\s])?([\"|\'])?(.*?)([\"|\'])?([\s])?\)/i';
				
					preg_match_all($pattern, $cssFile, $matches, PREG_PATTERN_ORDER);
					
					foreach ($matches[3] as $key => $match) {
						if(strpos($match, 'http') == false) {
							$fileType = null;
							$matchPathInfo = pathinfo($match);
							if(isset($matchPathInfo['extension']) && $matchPathInfo['extension'] == 'css') {
								$fileType = 'css';
							}
							$absolutePath = $this->getAbsolutePath($pathInfo['dirname'].'/'.$match);
							//Log::info("absolutePath:" . $absolutePath);
							//Log::info("fileType:" . $fileType);
							$this->storeFiles($domain, $absolutePath, $fileType);
						}
					}
				}
			}
			return $parseUrl['path'];
		}

		return '';
	}

}