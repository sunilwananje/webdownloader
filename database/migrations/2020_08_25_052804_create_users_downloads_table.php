<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_downloads', function (Blueprint $table) {
            $table->id();
            $table->char('uuid', 30);
            $table->string('url');
            $table->tinyInteger('pages');
            $table->string('theme_path');  
            $table->foreignId('user_id')
            ->nullable()
            ->constrained()
            ->onDelete('cascade');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_downloads');
    }
}
