<div class="modal fade bd-example-modal-lg" id="planModal" tabindex="-1" role="dialog" aria-labelledby="verifyModalContent2" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id ="planForm" action="javascript:void(0)" class="needs-validation" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="verifyModalContent2_title">Edit {{ $plan->name }} Plan</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label" for="recipient-name-1">Plan Name:</label>
                        <input class="form-control" name="plan_name" id="plan_name" type="text" value="{{ $plan->name }}">
                        <div class="invalid-feedback1" id="plan_name_error"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="recipient-name-1">Price:</label>
                        <input class="form-control" name="price" id="price" type="text" value="{{ $plan->price }}">
                        <div class="invalid-feedback1" id="price_error"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="message-text">Plan Description:</label>
                        <textarea class="form-control" name="plan_description" id="plan_description"> {{ $plan->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="interval_unit">Interval</label>
                        <select class="form-control {{ ($errors->has('interval_unit')) ?  'is-invalid' : '' }}" name="interval_unit" id="interval_unit">
                            <option value="">Select</option> 
                            <option value="month" @if ($plan->interval_unit == 'month') selected @endif>Month</option> 
                            <option value="year" @if ($plan->interval_unit == 'year') selected @endif>Year</option> 
                        </select>
                        <div class="invalid-feedback1" id="interval_unit_error"></div>
                    </div>

                    <div class="form-group">
                        <label for="sort_order">Sort Order</label>
                        <input class="form-control {{ ($errors->has('sort_order')) ?  'is-invalid' : '' }}" id="sort_order" type="number" name="sort_order" placeholder="Sort Order" value="{{$plan->sort_order }}" required="required">
                        <div class="invalid-feedback1" id="sort_order_error"></div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-1">
                            <label class="col-form-label" for="recipient-name-1">Sr No.</label>
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="recipient-name-1">Feature Name</label>
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="recipient-name-1">Value</label>
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="recipient-name-1">Interval</label>
                        </div>
                        <div class="form-group col-md-2">
                            <label class="col-form-label" for="recipient-name-1">Sort Order</label>
                        </div>
                        
                        <div class="form-group col-md-3">
                            <label class="col-form-label" for="message-text">Note</label>
                        </div>
                    </div>
                    @foreach($plan->features()->get() as $key => $feature)
                    <div class="row">
                        <div class="form-group col-md-1">
                            <label class="col-form-label" for="recipient-name-1">{{ $key + 1}}</label>
                        </div>
                        <div class="form-group col-md-2">
                            <!-- <label class="col-form-label" for="recipient-name-1">Feature Name:</label> -->
                            <input class="form-control" name="feature_name[]" id="feature_name" type="text" value="{{ $feature->name }}">
                            <div class="invalid-feedback1" id="feature_name.{{$key}}_error"></div>
                        </div>
                        <!-- <div class="form-group col-md-3">
                            <label class="col-form-label" for="message-text">Feature Description:</label>
                            <textarea class="form-control" name="feature_description" id="feature_description"> {{ $feature->description }}</textarea>
                        </div> -->
                        <div class="form-group col-md-2">
                            <!-- <label class="col-form-label" for="recipient-name-1">Value:</label> -->
                            <input class="form-control" name="value[]"  id="value" type="text" value="{{ $feature->pivot->value }}">
                            <div class="invalid-feedback1" id="value.{{$key}}_error"></div>
                        </div>
                        <div class="form-group col-md-2">
                            <select class="form-control {{ ($errors->has('interval_unit')) ?  'is-invalid' : '' }}" name="feture_interval_unit[]" id="feture_interval_unit">
                                <option value="">Select</option> 
                                <option value="day" @if ($feature->interval_unit == 'day') selected @endif>Day</option>
                                <option value="week" @if ($feature->interval_unit == 'week') selected @endif>Week</option>
                                <option value="month" @if ($feature->interval_unit == 'month') selected @endif>Month</option> 
                                <option value="year" @if ($feature->interval_unit == 'year') selected @endif>Year</option> 
                            </select>
                            <div class="invalid-feedback1" id="feture_interval_unit.{{$key}}_error"></div>
                        </div>

                        <div class="form-group col-md-2">
                            <input class="form-control {{ ($errors->has('feture_sort_order')) ?  'is-invalid' : '' }}" id="feture_sort_order" type="number" name="feture_sort_order[]" placeholder="Sort Order" value="{{$feature->sort_order }}" required="required">
                            <div class="invalid-feedback1" id="sort_order.{{$key}}_error"></div>
                        </div>

                        <div class="form-group col-md-3">
                            <!-- <label class="col-form-label" for="message-text">Note:</label> -->
                            <textarea class="form-control" name="note[]" id="note"> {{ $feature->pivot->note }}</textarea>
                            <div class="invalid-feedback1" id="note.{{$key}}_error"></div>
                        </div>
                        <input type="hidden" name="id[]" value="{{ $feature->pivot->id }}">
                        <input type="hidden" name="feature_id[]" value="{{ $feature->id }}">
                    </div>
                    @endforeach
                    <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit" id="planSubmit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>