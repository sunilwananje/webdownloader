<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Auth;

class FeatureValueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->input();
        switch($this->method())
            {
                case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                     'plan_id'     => 'required',
                     'feature_id'     => 'required',
                     'value'     => ['required', 
                                         Rule::unique('plan_features')->where(function ($query) use($input) {
                                            return $query->where('plan_id', $input['plan_id'])->where('feature_id', $input['feature_id']);
                                        })
                                     ],
                     'note' => 'required',
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {
                    return [
                     'plan_id'     => 'required',
                     'feature_id'     => 'required',
                     'value'     => 'required|',
                     'note' => 'required',
                    ];
                }
                default:break;
            } 
    }
}
