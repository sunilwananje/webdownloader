<!-- META TAGS -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keyword" content="">
<!-- FAV ICON(BROWSER TAB ICON) -->
<link rel="shortcut icon" href="{{ asset('assets/images/fav.ico') }}" type="image/x-icon">
<!-- GOOGLE FONT -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700%7CJosefin+Sans:600,700" rel="stylesheet">
<!-- FONTAWESOME ICONS -->
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
<!-- ALL CSS FILES -->
<link href="{{ asset('assets/css/materialize.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
<link href="{{ asset('assets/css/style-mob.css') }}" rel="stylesheet">