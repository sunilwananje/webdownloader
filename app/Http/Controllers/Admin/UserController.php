<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Custom\DownloadStatus;
use App\Models\UsersDownload;
use App\Models\Feature;
use App\Models\Plan;
use App\Models\PlanSubscription;
use App\Models\PlanSubscriptionUsage;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\UsersDownloadRepositoryInterface;
use App\User;
use Hash, Validator, DataTables, Session;

class UserController extends Controller
{
    private $userRepository;
    private $downloadRepository;
    private $downloadStatus;

    public function __construct(
        UserRepositoryInterface $userRepository,
        UsersDownloadRepositoryInterface $downloadRepository,
        DownloadStatus $downloadStatus
    ) {
        $this->downloadRepository = $downloadRepository;
        $this->downloadStatus = $downloadStatus;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all();
        if ($request->ajax()) {
            return $this->getUsersDataTable($users);
        }
        return view('admin.users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        try {
            $inputData = $request->all();
            $inputData['password'] = Hash::make($inputData['password']);
            $user = $this->userRepository->create($inputData);
            $plan = Plan::code(Plan::PLAN_FREE)->firstOrFail();
            $user->newSubscription('main', $plan)->create();
            $user->subscriptionUsage('main')->record(Feature::FEATURE_DOWNLOAD_SOURCE, 0);
            return redirect()->route('admin.users.index')->with('status', 'User added successfully!');
        } catch(Exception $e) {
            return redirect()->route('admin.users.index')->with('status', 'Something went wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        try {
            $user = $this->userRepository->getById($id);
            $subscription = null;
            $plan = 'Not Assigned';
            $planStatus = '';
            $endsAt = '';
            $consumed = 0;
            $total = 0;
            if ($user->subscription('main')) {
                $subscription = $user->subscription('main');
                $plan = $subscription->plan->name;
                $consumed = $subscription->ability()->consumed(Feature::FEATURE_DOWNLOAD_SOURCE);
                $remainings = $subscription->ability()->remainings(Feature::FEATURE_DOWNLOAD_SOURCE);
                $total = $subscription->ability()->value(Feature::FEATURE_DOWNLOAD_SOURCE);
                if ($subscription->isEnded()) {
                    $planStatus = 'Inactive';
                } else {
                    $planStatus = 'Active';
                }
                $endsAt = date('d M, Y', strtotime($subscription->ends_at));
            }
            $allStatus = $this->downloadStatus->listStatus();
            if ($request->ajax()) {
                $downloads = $this->downloadRepository->getByUser($user);
                return $this->getDownloadsDataTable($downloads);
            }
         return view('admin.user-profile', compact('user', 'subscription', 'plan', 'consumed', 'remainings', 'total', 'planStatus', 'endsAt'));
        } catch(Exception $e) {
            return [];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $user = $this->userRepository->getById($id);
            return view('admin.user-form', compact('user'));
        } catch(\Exception $e) {
            return view('admin.user-form');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        try {
            $inputData = $request->all();
            $inputData['password'] = Hash::make($inputData['password']);
            unset($inputData['_token']);
            unset($inputData['_method']);
            unset($inputData['password_confirmation']);
            $user = $this->userRepository->update($inputData, $id);
            return redirect()->route('admin.users.index')->with('status', 'User updated successfully!');
        } catch(\Exception $e) {
            return redirect()->route('admin.users.index')->with('status', 'Something went wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if ($this->userRepository->delete($id)) {
                return redirect()->route('admin.users.index')->with('status', 'User has been deleted');
            }
            return redirect()->route('admin.users.index')->with('error', 'Something went wrong.');
        } catch(\Exception $e) {
            return redirect()->route('admin.users.index')->with('error', 'Something went wrong.');
        }
    }

    /**
     * Assign Plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assignPlan($id)
    {
        try {
            $user = User::where('id', $id)->first();
            if ($user) {
                $subscription = null;
                $plan = null;
                $consumed = 0;
                $total = 0;
                if ($user->subscription('main')) {
                    $subscription = $user->subscription('main');
                    $plan = $subscription->plan->name;
                    $consumed = $subscription->ability()->consumed(Feature::FEATURE_DOWNLOAD_SOURCE);
                    $remainings = $subscription->ability()->remainings(Feature::FEATURE_DOWNLOAD_SOURCE);
                    $total = $subscription->ability()->value(Feature::FEATURE_DOWNLOAD_SOURCE);
                }

                $plans = Plan::all();
                return view('admin.assign-plan', compact('id', 'plans', 'user', 'subscription', 'plan', 'consumed', 'remainings', 'total'));
            }
            return redirect()->route('admin.users.index')->with('error', 'User not found');
        } catch(\Exception $e) {
            return redirect()->route('admin.users.index')->with('error', $e->getMessage());
        }

    }

    /**
     * Assign Plan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeAssignPlan($id, Request $request)
    {
        try {

            $input = $request->all();
            $rules['plan_state'] = 'required';
            $rules['plan_id'] = 'required';
//            if ($input['plan_state'] == 1) {
//                $rules['extend_by'] = 'required';
//            }
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->route('admin.assign.plan', $id)
                            ->withErrors($validator)
                            ->withInput();
            }

            $user = $this->userRepository->getById($id);
            $plan = Plan::where('id', $input['plan_id'])->firstOrFail();
            if ($plan) {
                if ($input['plan_state'] == 0) {
                    $user->newSubscription('main', $plan)->create();
                    $user->subscriptionUsage('main')
                        ->record(Feature::FEATURE_DOWNLOAD_SOURCE, 0, true, $plan->id);
                    $successMsg = 'Plan has been assigned';
                } else {
                    if ($user->subscription('main')) {
                        $planSubscription = PlanSubscription::where('id', $user->subscription('main')->id)
                            ->firstOrFail();
                        if (!empty($input['extend_by'])) {
                            $planSubscription->ends_at = date('Y-m-d H:i:s', strtotime($input['extend_by']));
                        }
                        $planSubscription->save();
                        $usage = PlanSubscriptionUsage::where('subscription_id', $planSubscription->id)
                            ->first();
                        if ($usage) {
                            $usage->valid_until = $planSubscription->ends_at;
                            if (!empty($input['download_value'])) {
                                $usage->total_value = $usage->total_value + $input['download_value'];
                            }
                            $usage->save();
                        }
                    }
                    $successMsg = 'Plan has been extended';
                }

                return redirect()->route('admin.users.index')->with('status', $successMsg);
            }
            return redirect()->route('admin.users.index')->with('status', 'Something went wrong.');
        } catch(\Exception $e) {
            return redirect()->route('admin.users.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Get DataTable Data.
     *
     * @param  array $downloads
     * @return \Illuminate\Http\Response
     */
    protected function getDownloadsDataTable($users)
    {
        return DataTables::of($users)
            ->addIndexColumn()
            ->editColumn('url', function($row) {
                    return '<a href="'.$row->url.'" target="_blank">'.$row->url.'</a>';
            })
            ->editColumn('pages', function($row) {
                    return (($row->pages == 1) ? 'All' : 'One');
            })
            ->editColumn('created_at', function($row) {
                    return date('d-m-Y H:i:s', strtotime($row->created_at));
            })
            ->editColumn('updated_at', function($row) {
                    return date('d-m-Y H:i:s', strtotime($row->updated_at));
            })
            ->editColumn('status', function($row) {
                    if ($row->status == 1) {
                        $label = '<label class="badge badge-success">Success</label>';
                    } elseif ($row->status == 2) {
                        $label = '<label class="badge badge-warning">Pending</label>';
                    } elseif ($row->status == 3) {
                        $label = '<label class="badge badge-danger">Fail</label>';
                    } else {
                        $label = '<label>N/A</label>';
                    }
                    return $label;
            })
            ->addColumn('action', function($row){
                
                if ($row->status == 1) {
                    $btn = '<a href="'.$row->theme_path.'" title="Preview" target="_blank">
                      <i class="i-Eye"></i>
                    </a>&nbsp;';
                    $btn = $btn.'<a href="'.route('zip.download', $row->id).'" title="Download">
                        <i class="i-Download-from-Cloud"></i>
                      </a> &nbsp;';
                } elseif ($row->status == 2) {
                    $btn = '<span>Pending<span>';
                } else {
                    $btn = '<span>N/A<span>';
                }

                return $btn;
            })
            ->rawColumns(['url', 'pages', 'status', 'action', 'created_at', 'updated_at'])
            ->make(true);
    }

   /**
     * Get DataTable Data.
     *
     * @param  array $downloads
     * @return \Illuminate\Http\Response
     */
    protected function getUsersDataTable($users)
    {
        return DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('plan', function($row) {
                if ($row->subscription('main')) {
                    return $row->subscription('main')->plan->name;
                }
                return 'Not Assigned';
            })
            ->addColumn('plan_ends_at', function($row) {
                if ($row->subscription('main')) {
                    return date('d-m-Y', strtotime($row->subscription('main')->ends_at));
                }
                return 'N/A';
            })
            ->addColumn('plan_status', function($row) {
                $label = 'N/A';
                if ($row->subscription('main')) {
                    if ($row->subscription('main')->isEnded()) {
                        $label = '<label class="badge badge-danger">Inactive</label>';
                    } else {
                        $label = '<label class="badge badge-success">Active</label>';
                    }
                }
                return $label;
            })
            ->editColumn('email_verified_at', function($row) {
                if ($row->email_verified_at) {
                    $label = '<label class="badge badge-success">Yes</label>';
                } else {
                    $label = '<label class="badge badge-danger">No</label>';
                }
                return $label;
            })
            ->editColumn('created_at', function($row) {
                return date('d-m-Y H:i:s', strtotime($row->created_at));
            })
            ->editColumn('updated_at', function($row) {
                return date('d-m-Y H:i:s', strtotime($row->updated_at));
            })
            ->addColumn('action', function($row){
                
                $btn = '<a class="text-primary mr-2" href="'.route('admin.assign.plan', $row->id).'" title="Assign Plan">
                            <i class="i-Add-User"></i>
                        </a>';
                $btn .= '<a class="text-success mr-2" href="'.route('admin.users.edit', $row->id) .'" title="Edit Profile">
                            <i class="i-Edit"></i>
                        </a>';
                $btn .= '<a class="text-info mr-2" href="'.route('admin.users.show', $row->id) .'" title="View Profile">
                            <i class="i-Eye"></i>
                        </a>';
                $btn .= '<a class="text-danger delete mr-2" href="#" data-id="'.$row->id . '" title="Delete User">
                            <i class="i-Close"></i>
                        </a>
                        <form id="delete-form-'.$row->id . '" action="'.route('admin.users.destroy', $row->id) .'" method="POST" style="display: none;">
                        <input type="hidden" name="_token" id="csrf-token" value="'. Session::token().'" />
                        <input type="hidden" name="_method" value="delete">
                        </form>';

                return $btn;
            })
            ->rawColumns(['email_verified_at', 'plan_status', 'action', 'created_at', 'updated_at'])
            ->make(true);
    }
}
