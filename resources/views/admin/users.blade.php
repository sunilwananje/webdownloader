@extends('admin.layouts.default')
@section('title') {{ __('Users') }} @endsection
@section('custom-css')
<link href="{{ asset('assets/admin/css/plugins/sweetalert2.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/admin/css/plugins/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger" role="alert">
        {{ session('error') }}
    </div>
@endif
    <div class="breadcrumb">
        <h1 class="mr-2">Users</h1>
        <ul>
            <li><a href="{{ route('admin.dashboard')}}">Dashboard</a></li>
            <li>Users</li>
        </ul>
        <span class="flex-grow-1"></span>
        <div class="float-left">
            <a href="{{ route('admin.users.create') }}" class="btn btn-primary btn-block">Add User</a>
        </div>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row">
        
        <div class="col-md-12">
            <div class="card text-left">
                <div class="card-body">
                    <h4 class="card-title mb-3">List of all Users </h4>
                   <!--  <p>Responsive tables allow tables to be scrolled horizontally with ease. Make any table responsive across all viewports by wrapping a <code>.table</code> with <code>.table-responsive</code>. Or, pick a maximum breakpoint with
                        which to have a responsive table up to by using <code>.table-responsive{-sm|-md|-lg|-xl}</code>.
                    </p> -->
                    <div class="table-responsive">
                        <table class="display table table-striped table-bordered" id="usersTable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Email Verified</th>
                                    <th scope="col">Plan</th>
                                    <th scope="col">Plan Ends At</th>
                                    <th scope="col">Plan Status</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col">Updated At</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main-content -->
@endsection

@section('custom-script')
<script src="{{ asset('assets/admin/js/plugins/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/plugins/datatables.min.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function () {
      $(document).on('click', '.delete', function () {
        var id = $(this).data('id');
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#0CC27E',
          cancelButtonColor: '#FF586B',
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success mr-5',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }).then(function () {
            event.preventDefault();
            $('#delete-form-'+id).submit();
        });
      });

      // DataTable
      $('#usersTable').DataTable({
         processing: true,
         serverSide: true,
         paging:true,
         serverMethod: 'post',
         pagingType: "full_numbers",
         ajax: "{!! route('admin.user.index.post', $user->id) !!}",
         columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'email_verified_at', name: 'email_verified_at' },
            { data: 'plan', name: 'plan' },
            { data: 'plan_ends_at', name: 'plan_ends_at' },
            { data: 'plan_status', name: 'plan_status' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
         ]
      });
    });
</script>
@endsection