@extends('admin.layouts.default')
@section('title') {{ __('Plans') }} @endsection
@section('custom-css')
<link href="{{ asset('assets/admin/css/plugins/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<?php 
$colors = [
    0 => 'indigo-400',
    1 => 'pink-400',
    2 => 'teal-400',
    3 => 'cyan-400',
    4 => '',
    5 => '',
];
?>
<div class="breadcrumb">
    <h1 class="mr-2">Plans</h1>
    <ul>
        <li><a href="{{ route('admin.dashboard')}}">Dashboard</a></li>
        <li>Plans</li>
    </ul>
    <span class="flex-grow-1"></span>
    <div class="float-left">
        <a href="{{ route('admin.plan.create') }}" class="btn btn-primary btn-block">Add Plan</a>
    </div>
</div>
<div class="separator-breadcrumb border-top"></div>
<!-- <div class="row">
    <?php //echo '<pre>'.print_r($users, true).'</pre>';?>
    <div class="col-md-12">
        <div class="card text-left">
            <div class="card-body">
                <h4 class="card-title mb-3">List of all Plans </h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Code</th>
                                <th scope="col">Description</th>
                                <th scope="col">Price</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Updated At</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plans as $plan)
                            <tr>
                                <th scope="row">{{ $plan->id }}</th>
                                <td>{{ $plan->name }}</td>
                                <td>{{ $plan->code }}</td>
                                <td>{{ $plan->description }}</td>
                                <td>{{ $plan->price }}</td>
                                <td>{{ $plan->created_at }}</td>
                                <td>{{ $plan->updated_at }}</td>
                                <td>
                                    <a class="text-success mr-2" href="{{ route('admin.plan.edit', $plan->id) }}">
                                        <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                                    </a>
                                    <a class="text-danger delete mr-2" href="#" data-id="{{ $plan->id }}">
                                        <i class="nav-icon i-File-Trash font-weight-bold"></i>
                                    </a>
                                    <form id="delete-form-{{ $plan->id }}" action="{{ route('admin.plan.destroy', $plan->id) }}" method="POST" style="display: none;">
                                        @method('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> -->
<section>
    <div class="row mb-4 mt-4">
        @foreach($plans as $key => $plan)
        <?php //var_dump($plan->features()->get());?>
        <div class="col-lg-4 col-md-4 mb-4 text-center">
            <div class="card o-hidden mb-4">
                <div class="mb-4 card-header {{ $colors[$key]}}">
                    <div class="card-title text-white">{{ $plan->name }}</div>
                    <h4 class="text-white">{{ $plan->price }}</h4>
                </div>
                <div class="ul-widget-app__browser-list">
                    @foreach($plan->features()->get() as $feature)
                        <?php //var_dump($feature->pivot);?>
                        <div class="ul-widget-app__browser-list-1 mb-4">
                            <span class="text-15 ml-4">{{ $feature->name }}</span>
                            <span class="badge badge-round-success pill mr-2">{{ $feature->pivot->value }}</span>
                        </div>
                    @endforeach
                </div>
                <a href="javascript:;" class="btn btn-default plan-modal" data-id="{{ $plan->id }}">Edit</a>&nbsp;
                <a class="text-danger delete" href="#" data-id="{{ $plan->id }}">
                    Delete
                </a>
                <form id="delete-form-{{ $plan->id }}" action="{{ route('admin.plan.destroy', $plan->id) }}" method="POST" style="display: none;">
                    @method('delete')
                    @csrf
                </form>
            </div>
        </div>
        @endforeach
    </div>
    <div id="modalContainer"></div>
</section>
<!-- end of main-content -->
@endsection

@section('custom-script')
<script src="{{ asset('assets/admin/js/plugins/sweetalert2.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.delete').on('click', function () {
            var id = $(this).data('id');
            swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#0CC27E',
              cancelButtonColor: '#FF586B',
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              confirmButtonClass: 'btn btn-success mr-5',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false
            }).then(function () {
                event.preventDefault();
                $('#delete-form-'+id).submit();
            });
        });

        $(document).on('click','.plan-modal',function(){
           var id = $(this).data('id');
           $.ajax({
                type:'post',
                url:'/admin/edit/plan/'+id,
                dataType:'html',
                success:function(data) {
                  $("#modalContainer").html(data);
                  $("#planModal").modal("show");
                }
            });
             
        });

        $(document).on('click', '#planSubmit', function(event) {
            event.preventDefault();
            var formData = $('#planForm').serialize();
            var url = "{{ route('admin.plan.update')}}";
           // alert(url+"\n"+formData);
            $.ajax({
                method: 'POST',
                url: url,
                dataType: 'json',
                data: formData,
                success: function(msg) {
                    console.log(msg)
                  var value = eval(msg);
                  if(value.status == 'success'){
                    window.location.href = "{{ route('admin.plan.index')}}";
                    return false;
                  }
                },
                error: function(response) {
                    $.each(response.responseJSON.errors, function(index, value) {
                        console.log(index+"---"+value)
                        console.log("#" +index+'_error');
                        $('#' +index+'_error').html(value);
                        $('#feature_name.1_error').html(value)
                        alert($('#' +index+'_error').text())
                    });
                     //$('#feature_name.1_error').text(response.responseJSON.errors.feature_name);
               //      $('#emailError').text(response.responseJSON.errors.email);
               //      $('#mobileNumberError').text(response.responseJSON.errors.mobile_number);
               //      $('#aboutError').text(response.responseJSON.errors.about);
               }

            });
        });
    });
</script>
@endsection