<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{ request()->is('admin/dashboard') ? 'active' : ''}}"><a class="nav-item-hold" href="{{ route('admin.dashboard') }}"><i class="nav-icon i-Bar-Chart"></i><span class="nav-text">Dashboard</span></a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ request()->routeIs('admin.users*') ? 'active' : '' }}"><a class="nav-item-hold" href="{{ route('admin.users.index') }}"><i class="nav-icon i-Administrator"></i><span class="nav-text">Users</span></a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item"><a class="nav-item-hold" href="{{ route('admin.plan.index') }}"><i class="nav-icon i-Money-2"></i><span class="nav-text">Pricing</span></a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>