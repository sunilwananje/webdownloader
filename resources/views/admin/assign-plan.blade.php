@extends('admin.layouts.default')
@section('title') {{ __('Assign Plan') }} @endsection
@section('custom-css')
<link href="{{ asset('assets/admin/css/plugins/gijgo.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/admin/css/plugins/sweetalert2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
    <div class="breadcrumb">
        <h1 class="mr-2">Users</h1>
        <ul>
            <li><a href="{{ route('admin.dashboard')}}">Dashboard</a></li>
            <li><a href="{{ route('admin.users.index')}}">Users</a></li>
            <li>{{ __('Assign Plan') }}</li>
        </ul>
        <span class="flex-grow-1"></span>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row">
        <div class="col-md-8">
            <h4>{{ __('Assign Plan') }}</h4>
            <div class="card mb-5">
                <div class="card-body">
                    <form action="{{ route('admin.assign.plan.save', $id) }}" class="needs-validation" method="POST">
                        @csrf
                        @if($subscription)
                        <?php 
                            if ($subscription->isEnded()) {
                                $planStatus = '<span class="badge badge-danger">Inactive</span>';
                            } else {
                                $planStatus = '<span class="badge badge-success">Active</span>';
                            }
                        ?>
                        <div class="form-group row">
                            {!! $planStatus !!}
                        </div>
                        <div class="form-group row">
                            <label for="paln">Plan Name:</label>
                            <label for="paln">{{ $plan }}</label>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3">Plan Expiry Date <strong>{{ date('d M, Y', strtotime($subscription->ends_at)) }}</strong></label>
                        </div>
                        <div class="form-group row">
                            <label for="downloaded">Download Consumed:</label>
                            <label for="downloaded"><strong>{{ $consumed }}</strong></label>
                        </div>
                        <div class="form-group row">
                            <label for="downloaded">Remaining Downloads Available:</label>
                            <label for="downloaded"><strong>{{ $remainings }}</strong></label>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3">Total Downloads in Plan:</label>
                            <label for="inputEmail3"><strong>{{ $total }}</strong></label>
                        </div>
                        @endif
                        <div class="form-group row">
                            <label class="radio radio-primary">
                                <input type="radio" name="plan_state" value="0" @if (!$subscription) checked @endif class="plan_state"><span>New</span><span class="checkmark"></span>
                            </label>
                            <label class="radio radio-primary">
                                <input type="radio" name="plan_state" value="1" @if ($subscription) checked @endif class="plan_state"><span>Renew</span><span class="checkmark"></span>
                            </label>
                            @error('plan_state')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group row" id="planSelect">
                            <label for="inputEmail3">Plan</label>
                            <select class="form-control {{ ($errors->has('plan_id')) ?  'is-invalid' : '' }}" name="plan_id" id="plan_id">
                                <option value="">Select</option>
                                @foreach($plans as $plan)
                                    <option value="{{ $plan->id }}" @if ($user->subscription('main') && $user->subscription('main')->plan_id == $plan->id) selected @endif>{{ $plan->name }}</option>
                                @endforeach
                            </select>
                            @error('plan_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div id="planContainer" style="display: none">
                            <div class="form-group row">
                                <label for="sort_order">Download value</label>
                                <input class="form-control" id="download_value" type="number" name="download_value" placeholder="Download value" value="{{ old('download_value') }}">
                            </div>
                            <div class="form-group row">
                                <label for="sort_order">Extend By</label>
                                <input class="form-control datepicker {{ ($errors->has('extend_by')) ?  'is-invalid' : '' }}" id="extend_by" type="text" name="extend_by" placeholder="Extend By" value="{{ old('extend_by') }}">
                                @error('extend_by')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main-content -->
@endsection

@section('custom-script')
<script src="{{ asset('assets/admin/js/plugins/gijgo.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
  'use strict'; // Fetch all the forms we want to apply custom Bootstrap validation styles to
    if ($('input[name="plan_state"]:checked').val() == 1) { 
        $('#planSelect').hide();
        //$('#planLabel').html('<label for="inputEmail3">'+$( "#plan_id option:selected" ).text()+'</label>')
        $('#planContainer').show();
    }
    $(document).on('click', '.plan_state', function() {
        if ($(this).val() == 1) {
            $('#planSelect').hide();
            $('#planContainer').show();
        } else {
            $('#planSelect').show();
            $('#planContainer').hide();
        }
    })

    var date = new Date();
    date.setDate(date.getDate()-1);
    $('.datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        minDate:date
    });
});
</script>
@endsection
