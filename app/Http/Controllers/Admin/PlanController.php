<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Requests\PlanRequest;
use App\Http\Requests\FeatureRequest;
use App\Http\Requests\FeatureValueRequest;
use App\Models\Plan;
use App\Models\Feature;
use App\Models\PlanFeature;
use Session;

class PlanController extends Controller
{

	public function index()
    {
    	$plans = Plan::all()->sortBy('sort_order');
    	return view('admin.plan.list', compact('plans'));
    }

    public function create(Request $request)
    {
    	$plans = Plan::all();
    	$features = Feature::all();
    	return view('admin.plan.create', compact('plans', 'features'));
    }

    public function edit($id)
    {
    	//$planFeature = PlanFeature::where('id', $id)->first();
    	$plan = Plan::where('id', $id)->first();
    	//$feature = Feature::where('id', $planFeature->feature_id)->first();
    	return view('admin.plan.edit', compact('plan'));
    }

    public function storePlan(PlanRequest $request)
    {
    	$input = $request->all();
    	$plan = Plan::create([
		    'name' => $input['name'],
		    'code' => strtolower(str_replace(' ', '-', $input['name'])),
		    'description' => $input['description'],
		    'price' => $input['price'],
		    'interval_unit' => $input['interval_unit'],
		    'interval_count' => 1,
		    'sort_order' => $input['sort_order'],
		]);
		return redirect()->route('admin.plan.create')->with('status', 'Plan added successfully!');
    }

    public function destroy($id)
    {
    	try {
            if (Plan::destroy($id)) {
                return redirect()->route('admin.plan.index')->with('status', 'Plan has been deleted');
            }
            return redirect()->route('admin.plan.index')->with('error', 'Something went wrong.');
        } catch(Exception $e) {
            return redirect()->route('admin.plan.index')->with('error', $e->getMessage());
        }
    }

    public function storeFeature(FeatureRequest $request)
    {
    	$input = $request->all();
    	$plan = Feature::create([
		    'name' => $input['name'],
		    'code' => strtolower(str_replace(' ', '-', $input['name'])),
		    'description' => $input['description'],
		    'interval_unit' => $input['interval_unit'],
		    'interval_count' => 1,
		    'sort_order' => $input['sort_order'],
		]);
		return redirect()->route('admin.plan.create')->with('status', 'Feature added successfully!');
    }

    public function storeFeatureValue(FeatureValueRequest $request)
    {
    	$input = $request->all();
    	$plan = Plan::where('id', $input['plan_id'])->first();
    	$feature = Feature::where('id', $input['feature_id'])->first();
		$plan->features()->attach([
		    $feature->id => [
		    	'value' => $input['value'],
		    	'note' => $input['note']
			]
		]);
		return redirect()->route('admin.plan.create')->with('status', 'Feature value added successfully!');
    }

    public function update(Request $request)
    {
    	$input = $request->all();
    	$fields['plan_id'] = $input['plan_id'];    
    	$fields['plan_id'] = 'required';
    	$fields['price'] = $input['price'];    
    	$fields['price'] = 'required';    
		$rules['plan_name'] = $input['plan_name'];
		$rules['plan_name'] = 'required|unique:plans,name,'.$input['plan_id'];
    	foreach ($input['feature_id'] as $key => $value) {
			$fields['feature_id.'.$key] = $value;    
			$rules['feature_id.'.$key] = 'required';
		}
		foreach ($input['feature_name'] as $key => $value) {
			$fields['feature_name.'.$key] = $value;    
			$rules['feature_name.'.$key] = 'required|unique:features,name,'.$input['feature_id'][$key];
		}
		foreach ($input['value'] as $key => $value) {
			$fields['value.'.$key] = $value;    
			$rules['value'] = [
	           	'required',
	           	Rule::unique('plan_features')
	           	->where('plan_id', $input['plan_id'])
	           	->where('feature_id', $input['feature_id'][$key])
	           	->ignore($input['id'][$key], 'id')
            ];
		}
		foreach ($input['note'] as $key => $value) {
			$fields['note.'.$key] = $value;    
			$rules['note.'.$key] = 'required';
		}
		
    	$this->validate($request, $rules);
	    if ($validator) {
			return response()->json(['error'=>$validator->errors()->all()]);
        }
        $plan = Plan::findOrFail($input['plan_id']);
        $plan->name = $input['plan_name'];
        $plan->price = $input['price'];
        $plan->interval_unit = $input['interval_unit'];
        $plan->sort_order = $input['sort_order'];
        $plan->save();
        foreach ($input['feature_id'] as $key => $value) {
        	$feature = Feature::findOrFail($value);
	        $feature->name = $input['feature_name'][$key];
	        $feature->interval_unit = $input['feture_interval_unit'][$key];
        	$feature->sort_order = $input['feture_sort_order'][$key];
	        $feature->save();
        }
        foreach ($input['id'] as $key => $value) {
        	$planFeature = PlanFeature::findOrFail($value);
	        $planFeature->value = $input['value'][$key];
	        $planFeature->note = $input['note'][$key];
	        $planFeature->save();
        }
        Session::flash('status', $input['plan_name'].' plan updated successfully');
		return response()->json(['status' => 'success']);	
    }

}
