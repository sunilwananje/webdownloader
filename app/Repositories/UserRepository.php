<?php

namespace App\Repositories;

use App\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Schema;

class UserRepository implements UserRepositoryInterface
{
	/**
     * @param array $columns
     * @return mixed
     */
    public function all()
    {
        return User::all();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return User::create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id") {
    	$userColumns = Schema::getColumnListing('users');
    	$user = User::find($id);
    	foreach ($data as $key => $value) {
            if (in_array($key, $userColumns)) {
                $user->$key = $value;
            }
        }
        return $user->save();
        //return User::where($attribute, '=', $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return User::where('id', $id)->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        return User::destroy($id);
    }
}