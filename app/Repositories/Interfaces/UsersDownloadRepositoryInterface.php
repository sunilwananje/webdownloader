<?php

namespace App\Repositories\Interfaces;

use App\User;
use App\Models\UsersDownload;

interface UsersDownloadRepositoryInterface
{
    public function all($take = null);

   	public function download(UsersDownload $usersDownload);
   	
   	public function update($id, array $attributes);

    public function delete($id);

    public function show($id);

    public function getByUser(User $user, $take = null);
}