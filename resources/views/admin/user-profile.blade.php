@extends('admin.layouts.default')
@section('title') {{ __('Dashboard') }} @endsection
@section('custom-css')
<link href="{{ asset('assets/admin/css/plugins/datatables.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
    <div class="breadcrumb">
        <h1>User Profile</h1>
        <ul>
            <li><a href="{{ route('admin.dashboard')}}">Dashboard</a></li>
            <li><a href="{{ route('admin.users.index')}}">Users</a></li>
            <li>User Profile</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="card user-profile o-hidden mb-4">
        <div class="header-cover" style="background-image: url('{{ asset('assets/admin/images/photo-wide-4.jpg')}}')"></div>
        <div class="user-info"><img class="profile-picture avatar-lg mb-2" src="{{ asset('assets/admin/images/faces/1.jpg') }}" alt="">
            <p class="m-0 text-24">{{ $user->name }}</p>
            <p class="text-muted m-0">Digital Marketer</p>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs profile-nav mb-4" id="profileTab" role="tablist">
                <!-- <li class="nav-item"><a class="nav-link active" id="timeline-tab" data-toggle="tab" href="#timeline" role="tab" aria-controls="timeline" aria-selected="false">Timeline</a></li> -->
                <li class="nav-item"><a class="nav-link active" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="true">About</a></li>
                <li class="nav-item"><a class="nav-link" id="friends-tab" data-toggle="tab" href="#friends" role="tab" aria-controls="friends" aria-selected="false">Downloads</a></li>
            </ul>
            <div class="tab-content" id="profileTabContent">
                <div class="tab-pane fade active show" id="about" role="tabpanel" aria-labelledby="about-tab">
                    <h4>Personal Information</h4>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, commodi quam! Provident quis voluptate asperiores ullam, quidem odio pariatur. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatem, nulla eos?
                        Cum non ex voluptate corporis id asperiores doloribus dignissimos blanditiis iusto qui repellendus deleniti aliquam, vel quae eligendi explicabo.
                    </p>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-6">
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Calendar text-16 mr-1"></i> Birth Date</p><span>1 Jan, 1994</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Edit-Map text-16 mr-1"></i> Birth Place</p><span>Dhaka, DB</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Globe text-16 mr-1"></i> Lives In</p><span>Dhaka, DB</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-MaleFemale text-16 mr-1"></i> Gender</p><span>1 Jan, 1994</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-MaleFemale text-16 mr-1"></i> Email</p><span>{{ $user->email}}</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Cloud-Weather text-16 mr-1"></i> Website</p><span>www.ui-lib.com</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Face-Style-4 text-16 mr-1"></i> Profession</p><span>Digital Marketer</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Professor text-16 mr-1"></i> Experience</p><span>8 Years</span>
                            </div>
                            <div class="mb-4">
                                <p class="text-primary mb-1"><i class="i-Home1 text-16 mr-1"></i> School</p><span>School of Digital Marketing</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4>Plan Info</h4>
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="i-Gift-Box text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Plan</h4>
                                    <span>{{ $plan }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="i-Calendar text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Expire on</h4>
                                    <span>{{ $endsAt}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="i-Data-Download text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Downloaded</h4>
                                    <span>{{ 'Total:'. $consumed}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-6">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="i-Data-Download text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Total In Plan</h4>
                                    <span>{{ $remainings}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4>Other Info</h4>
                    <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum dolore labore reiciendis ab quo ducimus reprehenderit natus debitis, provident ad iure sed aut animi dolor incidunt voluptatem. Blanditiis, nobis aut.</p>
                    <div class="row">
                        <div class="col-md-2 col-sm-4 col-6 text-center"><i class="i-Plane text-32 text-primary"></i>
                            <p class="text-16 mt-1">Travelling</p>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6 text-center"><i class="i-Camera text-32 text-primary"></i>
                            <p class="text-16 mt-1">Photography</p>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6 text-center"><i class="i-Car-3 text-32 text-primary"></i>
                            <p class="text-16 mt-1">Driving</p>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6 text-center"><i class="i-Gamepad-2 text-32 text-primary"></i>
                            <p class="text-16 mt-1">Gaming</p>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6 text-center"><i class="i-Music-Note-2 text-32 text-primary"></i>
                            <p class="text-16 mt-1">Music</p>
                        </div>
                        <div class="col-md-2 col-sm-4 col-6 text-center"><i class="i-Shopping-Bag text-32 text-primary"></i>
                            <p class="text-16 mt-1">Shopping</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="friends" role="tabpanel" aria-labelledby="friends-tab">
                    <div class="row">
                        <div class="card text-left">
                            <div class="card-body">
                                <h4 class="card-title mb-3">Downloads</h4>
                                <p>
                                    The default page control presented by DataTables (forward and backward buttons with up to 7 page numbers in-between) is fine for most situations, but there are cases where you may wish to customise the options presented to
                                    the end user. This is done through DataTables' extensible pagination mechanism, the <a href="//datatables.net/reference/option/pagingType"><code class="option" title="DataTables initialisation option">pagingType</code></a> option.
                                </p>
                                <div class="table-responsive">
                                    <table class="display table table-striped table-bordered" id="downloadTable" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>#Sr.No</th>
                                                <th>Url</th>
                                                <th>No of Pages</th>
                                                <th>Status</th>
                                                <th>Created At</th>
                                                <th>Updated At</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end of main-content -->
@endsection
@section('custom-script')
<script src="{{ asset('assets/admin/js/plugins/datatables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

      // DataTable
      $('#downloadTable').DataTable({
         processing: true,
         serverSide: true,
         paging:true,
         serverMethod: 'post',
         pagingType: "full_numbers",
         ajax: "{!! route('admin.users.show.post', $user->id) !!}",
         columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'url', name: 'url' },
            { data: 'pages', name: 'pages' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
         ]
      });

    });
</script>
@endsection