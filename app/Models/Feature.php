<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use App\Models\Concerns\HasCode;
use App\Models\Concerns\Resettable;

/**
 * Class Feature
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Feature extends Model
{
    use Resettable;

    const FEATURE_UPLOAD_IMAGES = 'upload-images';
    const FEATURE_UPLOAD_VIDEO = 'upload-video';
    const FEATURE_DOWNLOAD_SOURCE = 'download-source-code';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'description',
        'interval_unit',
        'interval_count',
        'sort_order',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Plan constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable('features');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function plans()
    {
        return $this->belongsToMany(
            'App\Models\Plan',
            'App\Models\PlanFeature',
            'feature_id',
            'plan_id'
        )->using('App\Models\PlanFeature');
    }

    /**
     * Get feature usage.
     *
     * This will return all related subscriptions usages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usage()
    {
        return $this->hasMany(
            'App\Models\PlanSubscriptionUsage',
            'feature_code',
            'code'
        );
    }
}
