<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UsersDownload;
use App\Repositories\Interfaces\UsersDownloadRepositoryInterface;
use App\Jobs\ExtractDataJob;
use App\Models\Feature;
use Auth, Madzipper;

class UsersDownloadController extends Controller
{
    private $downloadRepository;

    public function __construct(UsersDownloadRepositoryInterface $downloadRepository)
    {
        $this->downloadRepository = $downloadRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $downloads = $this->downloadRepository->all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getByUser()
    {
        $downloads = $this->downloadRepository->getByUser(Auth::user());
        return view('frontend.users-download', ['downloads' => $downloads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $inputData = $request->all();

        $request->validate([
            'url' => 'required|url'
        ]);
        $user = Auth::user();
        $inputData['user_id'] = $user->id;
        $inputData['uuid'] = bin2hex(random_bytes(10));
        $inputData['status'] = 2; //for pending
        $downloadObj = UsersDownload::create($inputData);
        $user->subscriptionUsage('main')->record(Feature::FEATURE_DOWNLOAD_SOURCE);
        dispatch(new ExtractDataJob($this->downloadRepository, $downloadObj));
        return redirect()->back()->with('success', 'Uploaded');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsersDownload  $usersDownload
     * @return \Illuminate\Http\Response
     */
    public function show(UsersDownload $usersDownload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsersDownload  $usersDownload
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersDownload $usersDownload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsersDownload  $usersDownload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersDownload $usersDownload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsersDownload  $usersDownload
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersDownload $usersDownload)
    {
        //
    }

    public function downloadZip($id)
    {
        $theme = UsersDownload::findOrFail($id);
        if($theme) {
            $user = Auth::user();
            //dd($user->subscription('main')->ability()->consumed(Feature::FEATURE_DOWNLOAD_SOURCE));
            $sourcePath = storage_path('preview/uid_' . $theme->user_id . '/' . $theme->uuid);
            $files = $sourcePath;
            $zipPath = storage_path('preview/uid_' . $theme->user_id) .'/'.$theme->uuid.'.zip';
            Madzipper::make($zipPath)->add($sourcePath)->close();
            return response()->download($zipPath);;
        }
    }
}
