<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class PlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
            {
                case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                     'name'     => 'required',
                     'description'     => 'required',
                     'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {
                    return [    
                     'name'     => 'required',
                     'description'     => 'required',
                     'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                    ];
                }
                default:break;
            }    
    }
}
