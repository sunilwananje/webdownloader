<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Models\Feature;
use App\Repositories\Interfaces\UsersDownloadRepositoryInterface;

class ExtractDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $downloadRepository;
    private $userDownload;
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(UsersDownloadRepositoryInterface $downloadRepository, $userDownload)
    {
        $this->downloadRepository = $downloadRepository;
        $this->userDownload = $userDownload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Queue Job Statred.');
        Log::info('$this->userDownload->url =' .$this->userDownload->url);
        
        $themeUrl = $this->downloadRepository->download($this->userDownload);
        Log::info('$themeUrl='.$themeUrl);
        if($themeUrl) {
            $inputData['theme_path'] = $themeUrl;
            $inputData['status'] = 1; //for success
        } else {
            Log::info('Job Failed');
            $user = User::where('id', $this->userDownload->user_id)->first();
            $user->subscriptionUsage('main')->reduce(Feature::FEATURE_DOWNLOAD_SOURCE);
            $inputData['status'] = 3; //for failed
        }
        $this->downloadRepository->update($this->userDownload->id, $inputData);
        Log::info('Queue Job End.');
    }
}
