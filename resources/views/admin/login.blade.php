<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Signin | Admin Template</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
        <link href="{{ asset('assets/admin/css/themes/lite-purple.min.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="auth-layout-wrap" style="background-image: url({{ asset('assets/admin/images/photo-wide-4.jpg') }})">
            <div class="auth-content">
                <div class="card o-hidden">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="p-4">
                                <div class="auth-logo text-center mb-4"><img src="{{ asset('assets/admin/images/logo.png') }}" alt=""></div>
                                @if (session('error'))
                                    <div class="alert alert-card alert-danger" role="alert">
                                        {{ session('error') }}
                                    </div>
                                @endif
                                <h1 class="mb-3 text-18">Sign In</h1>
                                <form method="POST" action="{{ route('admin.check.login') }}" class="needs-validation">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input id="email" type="email" class="form-control form-control-rounded @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autofocus>
                                        @error('email')
                                            <span class="invalid-feedback">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input id="password" type="password" class="form-control form-control-rounded @error('password') is-invalid @enderror" name="password" required>
                                        @error('password')
                                            <span class="invalid-feedback">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-rounded btn-primary btn-block mt-2">Sign In</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6 text-center" style="background-size: cover;background-image: url({{ asset('assets/admin/images/photo-long-3.jpg') }})">
                            <!-- <div class="pr-3 auth-right"><a class="btn btn-rounded btn-outline-primary btn-outline-email btn-block btn-icon-text" href="signup.html"><i class="i-Mail-with-At-Sign"></i> Sign up with Email</a><a class="btn btn-rounded btn-outline-google btn-block btn-icon-text"><i class="i-Google-Plus"></i> Sign up with Google</a><a class="btn btn-rounded btn-block btn-icon-text btn-outline-facebook"><i class="i-Facebook-2"></i> Sign up with Facebook</a></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('assets/admin/js/plugins/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('assets/admin/js/plugins/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/admin/js/plugins/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('assets/admin/js/scripts/script.min.js') }}"></script>
        <script src="{{ asset('assets/admin/js/scripts/sidebar.large.script.min.js') }}"></script>
        <script src="{{ asset('assets/admin/js/scripts/form.validation.script.min.js') }}"></script>
    </body>
</html>

