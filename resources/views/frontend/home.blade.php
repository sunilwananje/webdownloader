@extends('frontend.layouts.userapp')

@section('content')
<div class="udb">
  <div class="udb-sec udb-cour-stat">
    @if (session('success'))
      <div class="alert alert-success" role="alert">
          {{ session('success') }}
      </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

      <div class="box-inn-sp admin-form">
          <div class="tab-inn">
            @if($remainings > 0)
              <form action="{{ route('download') }}" method="post">
                  @csrf
                  <div class="row">
                      <div class="input-field col s12">
                          <input type="text" id="url" placeholder="http://example.com" name="url" class="validate">
                          <p> <strong><?php echo $consumed ." website downloaded out of ".$value; ?></strong></p>
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col s6">
                          <input class="validate" type="radio" name="pages" id="option1" value="0" checked>
                          <label for="option1">One Page</label>
                      </div>
                      <div class="input-field col s6">
                          <input class="validate" type="radio" name="pages" id="option2" value="1">
                          <label for="option2">All Pages</label>
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col s12">
                          <i class="waves-effect waves-light btn-large waves-input-wrapper" style=""><input type="submit" class="waves-button-input"></i>
                      </div>
                  </div>
              </form>
            @else
            <p> <strong><?php echo $consumed ." website downloaded out of ".$value; ?></strong></p>
            @endif
          </div>
      </div>
  </div>
  <div class="udb-sec udb-cour-stat">
      <h4><img src="{{ asset('assets/images/icon/db3.png') }}" alt=""> My Latest Downloads</h4>
      <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.The point of using Lorem Ipsummaking it look like readable English.</p>
      <div class="pro-con-table table-responsive">
          <table class="bordered responsive-table">
              <thead>
                  <tr>
                      <th>#Id</th>
                      <th>Url</th>
                      <th>No of Pages</th>
                      <th>Status</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($downloads as $download)
                  @if($download->status == 1)
                  <?php $class = 'label-success'; ?>
                  @elseif($download->status == 2)
                  <?php $class = 'label-warning'; ?>
                  <?php $link =  'Wait..'; ?>
                  @elseif($download->status == 3)
                  <?php $class = 'label-danger'; ?>
                  <?php $link =  'N/A'; ?>
                  @endif
                  <tr>
                      <td>{{ $download->id }}</td>
                      <td>
                        <a href="{{ $download->url }}" target="_blank">{{ $download->url }}</a>
                      </td>
                      <td>{{ (($download->pages == 1) ? 'All' : 'One') }}</td>
                      <td class="label {{$class}}">
                      {{ isset($allStatus[$download->status]) ? $allStatus[$download->status] : '' }}
                      </td>
                      <td>{{ $download->created_at }}</td>
                      <td>{{ $download->updated_at }}</td>
                      <td>
                        @if($download->status == 1)
                        <a href="{{ $download->theme_path }}" title="Preview" target="_blank">
                          <i class="fa fa-eye"></i>
                        </a>&nbsp;
                          <a href="{{ route('zip.download', $download->id) }}" title="Download">
                            <i class="fa fa-download"></i>
                          </a> &nbsp;
                        @else
                          {{$link}}
                        @endif
                      </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
      </div>
  </div>
</div>
@endsection
