<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    @include('frontend.includes.head')

  </head>
  <body>
    <!--== MAIN CONTRAINER ==-->
    <!-- Navbar -->
    @include('frontend.includes.header')
    <!-- End Navbar -->
    <section>
    <div class="pro-cover">
    </div>
    <div class="pro-menu">
        @include('frontend.includes.sidebar')
    </div>
    <div class="stu-db">
        <div class="container pg-inn">
            <div class="col-md-3">
                <div class="pro-user">
                    <img src="{{ asset('assets/images/user.jpg') }}" alt="user">
                </div>
                <div class="pro-user-bio">
                    <ul>
                        <li>
                            <h4>Emily Jessica</h4>
                        </li>
                        <li>Student Id: ST17241</li>
                        <li><a href="#!"><i class="fa fa-facebook"></i> Facebook: my sample</a></li>
                        <li><a href="#!"><i class="fa fa-google-plus"></i> Google: my sample</a></li>
                        <li><a href="#!"><i class="fa fa-twitter"></i> Twitter: my sample</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                 @yield('content')
            </div>
        </div>
    </div>
</section>
  @include('frontend.includes.footer')
  <script src="{{ asset('assets/js/main.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/materialize.min.js') }}"></script>
  <script src="{{ asset('assets/js/custom.js') }}"></script>
  </body>
</html>