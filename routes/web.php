<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function(){
	
	Route::middleware('admin')->group(function(){
	    Route::get('dashboard', function () {
		    return view('admin.dashboard');
		})->name('dashboard');
		Route::resource('users', 'UserController');
		Route::get('/plan','PlanController@index')->name('plan.index');
		Route::get('/create/plan','PlanController@create')->name('plan.create');
		Route::post('/edit/plan/{id}','PlanController@edit')->name('plan.edit');
		Route::post('/update/plan','PlanController@update')->name('plan.update');
		Route::delete('/destroy/plan/{id}','PlanController@destroy')->name('plan.destroy');
		Route::post('/users/list','UserController@index')->name('user.index.post');
		Route::post('/users/{id}','UserController@show')->name('users.show.post');
		Route::get('/assign/plan/{id}','UserController@assignPlan')->name('assign.plan');
		Route::post('/assign/plan/{id}','UserController@storeAssignPlan')->name('assign.plan.save');
		Route::post('/store/plan','PlanController@storePlan')->name('plan.store');
		Route::post('/store/feature','PlanController@storeFeature')->name('feature.store');
		Route::post('/store/featureValue','PlanController@storeFeatureValue')->name('featureValue.store');
	});

	Route::namespace('Auth')->group(function(){
        
	    //Login Routes
	    Route::get('/login','LoginController@showLoginForm')->name('login');
	    Route::post('/login','LoginController@login')->name('check.login');
	    Route::post('/logout','LoginController@logout')->name('logout');

	    //Forgot Password Routes
	    Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
	    Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

	    //Reset Password Routes
	    Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
	    Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');

	});
});


Auth::routes();

Route::middleware('auth')->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/download','UsersDownloadController@store')->name('download');
    Route::get('/all/downloads','UsersDownloadController@index')->name('all.download');
    Route::get('/my-downloads','UsersDownloadController@getByUser')->name('my.download');
    Route::get('/download/{id}','UsersDownloadController@downloadZip')->name('zip.download');
});
Route::redirect('/admin', '/admin/login');

