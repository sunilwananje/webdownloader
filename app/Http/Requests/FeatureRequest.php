<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class FeatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
            {
                case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                     'name'     => 'required',
                     'description'     => 'required',
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {
                    return [    
                     'name'     => 'required',
                     'description'     => 'required',
                    ];
                }
                default:break;
            } 
    }
}
