<?php

namespace App\Http\Controllers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\UserRepository;
use App\Repositories\UsersDownloadRepository;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\UsersDownloadRepositoryInterface;
use App\Models\UsersDownload;
use App\Models\PlanSubscription;
use App\Custom\DownloadStatus;
use App\Models\Feature;
use Auth;

class HomeController extends Controller
{
    private $downloadRepository;
    private $downloadStatus;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(UsersDownloadRepositoryInterface $downloadRepository, DownloadStatus $downloadStatus) {
        $this->downloadRepository = $downloadRepository;
        $this->downloadStatus = $downloadStatus;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $allStatus = $this->downloadStatus->listStatus();
        $downloads = $this->downloadRepository->getByUser($user, 5);
        $isPlanActive = false;
        $consumed = 0;
        $remainings = 0;
        $value = 0;
        if ($user->subscription('main')) {
            $subscription = PlanSubscription::bySubscriber($user)->first();

            if ($user->subscription('main')->isActive()) {
                $isPlanActive = $user->subscription('main')->ability()->canUse(Feature::FEATURE_DOWNLOAD_SOURCE);
                $consumed = $user->subscription('main')->ability()->consumed(Feature::FEATURE_DOWNLOAD_SOURCE);
                $remainings = $user->subscription('main')->ability()->remainings(Feature::FEATURE_DOWNLOAD_SOURCE);
                $value = (int)$user->subscription('main')->ability()->value(Feature::FEATURE_DOWNLOAD_SOURCE);
            }
        }
        //dd($consumed, $remainings, $value, $isPlanActive);
        return view('frontend.home', compact('downloads', 'allStatus', 'user', 'isPlanActive', 'consumed', 'remainings', 'value'));
        //return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function process()
    {
        return view('frontend.home');
    }
}
