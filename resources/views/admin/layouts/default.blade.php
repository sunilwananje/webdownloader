<!doctype html>
<html lang="en">
  <head>
    <title>@yield('title')</title>
    <!-- Required meta tags -->
    @include('admin.includes.head')
    @yield('custom-css')
  </head>
  <body class="text-left">
    <div class="app-admin-wrap layout-sidebar-large">
      <!-- main-header -->
      @include('admin.includes.header')
      <!-- End main-header -->
      <!-- sidebar -->
      @include('admin.includes.sidebar')
      <!-- End sidebar -->
      <div class="main-content-wrap sidenav-open d-flex flex-column">
        <div class="main-content">
          @yield('content')
        </div>
        <!-- Footer Start -->
        @include('admin.includes.footer')
        <!-- fotter end -->
      </div>
    </div> 
    @include('admin.includes.script')
    @yield('custom-script')
  </body>
</html>