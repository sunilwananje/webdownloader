<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    @include('frontend.includes.head')

  </head>
  <body>
    <!--== MAIN CONTRAINER ==-->
    <!-- Navbar -->
    @include('frontend.includes.header')
    <!-- End Navbar -->
    @yield('content')
  @include('frontend.includes.footer')
  <script src="{{ asset('assets/js/main.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/materialize.min.js') }}"></script>
  <script src="{{ asset('assets/js/custom.js') }}"></script>
  </body>
</html>